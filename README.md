# AI2Helper

An easy way to operate blocks at MIT App Inventor 2.

## main function:

1. find blocks by key work
1. remove all comments
1. download top blocks seperately (ignore orphan blocks)
1. outline of all top blocks
1. make the panels to scroll vertically seperately.

## HOW TO INSTALL


1. go to [tampermonkey](https://www.tampermonkey.net/) , install the browser extension according to your browser.
1. go to [App Inventer 2 block helper (greasyfork.org)](https://greasyfork.org/zh-CN/scripts/463114-app-inventer-2-block-helper) , to install this userscript.
1. open [https://ai2.appinventer.mit.edu](https://login.appinventer.mit.edu/login) , open your project (if autoload is distabled), in about 10 seconds, you will see the buttons as below:
 ![photo](https://gitee.com/ainote/ai2helper/raw/master/Snipaste_2024-04-02_20-31-19.jpg)
1. done.




for more info, please check this thread:
[DISCUSS ON COMMUITY OF AI2](https://community.appinventor.mit.edu/t/chrome-extension-ai2helper-download-all-blocks-seperately-and-remove-all-orphan-blocks-with-one-click/51767/88?u=kevinkun)